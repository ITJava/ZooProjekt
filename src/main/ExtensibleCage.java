package main;

import animals.Animal;
import animals.Herbivore;
import animals.Predator;
import food.Food;
import input.Input;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

import static main.Main.scan;

public class ExtensibleCage<T extends Animal> implements Animal.IDeadAnimalListener {
    private List<T> listCage = new CopyOnWriteArrayList<>();


    public List<T> getCage() {
        return listCage;
    }

    public int addAnimal(Animal ob) {
        listCage.add((T) ob);
        ob.setDeadAnimalListener(this);
        return listCage.size();
    }

    public boolean removeAnimal(int count) {
        if (count >= listCage.size() || count < 0) {
            System.out.println("Animal with this # is not exist");
            return false;
        }
        return listCage.remove(count) != null;
    }

    public void feedAnimal() {
        if (listCage.size() == 0) {
            System.out.println("Your cage is empty, fill it or leave");
        } else {
            while (true) {
                int count = cageAnimalIdCheck()-1;
                if (count < listCage.size() && count >= 0) {
                    Food food;
                    if(Math.random()>0.5){
                        food=Food.CARROT;
                    }else{
                        food=Food.MEAT;
                    }
                    System.out.println("You feed animal #" + count +
                            ". Current fill status is - " +
                            listCage.get(count).feed(food));
                    return;
                } else {
                    System.out.println("Try again or press enter exit");
                    if (scan.nextLine().equalsIgnoreCase("exit")) {
                        return;
                    }
                }
            }
        }
    }

    public int cageAnimalIdCheck() {
        return (int) Input.megaInputNumber("Choose animal from 1 to " + listCage.size());
    }

    public void checkHuntConditions(Animal animals) {
        if (animals instanceof Predator) {
            for (T t : listCage) {
                if (t instanceof Herbivore) {
                    ((Predator) animals).consume((Herbivore) t);
                    return;
                }
            }
        } else if (animals instanceof Herbivore) {
            for (T t : listCage) {
                if (t instanceof Predator) {
                    ((Predator) t).consume((Herbivore) animals);
                    return;
                }
            }
        } else {
            throw new RuntimeException("Wrong");
        }
    }

    public String getCageInfo() {
        sortAnimals();
        StringBuilder sb = new StringBuilder();
        Iterator<? extends Animal> it = listCage.iterator();
        while (it.hasNext()) {
            Animal animal = it.next();
            if (animal.getNickname() == null || animal.getNickname().length() == 0) {
                it.remove();
            } else {
                sb.append(animal.toString()).append("\n");
            }
        }
        return sb.toString();
    }

    private void sortAnimals() {
        Collections.sort(listCage);
    }

    @Override
    public void onAnimalDeath(Animal animal) {
        for (int i = 0; i < listCage.size(); i++) {
            if (listCage.get(i).equals(animal)) {
                System.out.println(animal.toString()+" - Died");
                removeAnimal(i);
                return;
            }
        }
    }
}