package animals;

public abstract class Herbivore extends Land {
    public Herbivore(double size, String nickname) {
        super(size, nickname);
    }

    public void onConsumption() {
        die();
    }
}
