package food;

public enum Food {
    CARROT(20), MEAT(40);
    double energyValue = 10;


    Food(double energyValue){
        this.energyValue=energyValue;

    }

    public double getEnergyValue() {
        return energyValue;
    }
}
