package main;

import animals.*;
import error.AnimalCreationException;
import error.AnimalInvalidNameException;
import error.AnimalInvalidSizeException;
import food.Food;
import input.Input;
import interfaces.Soundable;
import io.MyLogger;
import io.XmlImportExport;
import life.LifeChecker;
import org.pmw.tinylog.Configurator;
import org.pmw.tinylog.Level;
import org.pmw.tinylog.Logger;
import org.pmw.tinylog.writers.ConsoleWriter;
import org.pmw.tinylog.writers.FileWriter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Main {
    private Map<String, ExtensibleCage<? extends Animal>> cages;
    public static final Scanner scan = new Scanner(System.in);

    private Main() {
        cages = XmlImportExport.loadFromXml();
        LifeChecker lc= LifeChecker.getLifeChecker();
        lc.startWork(cages);

        boolean check = true;
        while (check) {
            System.out.println("Choose your action\n" +
                    "1. Check statistics\n" +
                    "2. Feeding protocol\n" +
                    "3. Create from nothing\n" +
                    "4. Let's jump\n" +
                    "5. Animal removal\n" +
                    "6. Import from file\n" +
                    "0. Exit the program");
            String choice = scan.nextLine();
            switch (choice) {
                case "1": {
                    showAnimalInfo();
                    break;
                }
                case "2": {
                    chooseCageToChange().feedAnimal();
                    break;
                }
                case "3": {
                    sortToCages();
                    break;
                }
                case "4": {
                    jumpAll();
                    break;
                }
                case "5": {
                    chooseCageToChange().removeAnimal(chooseCageToChange().cageAnimalIdCheck());
                    break;
                }
                case "6": {
                    List<Animal> listAnimal;
                    try {
                        listAnimal = Input.importFromFile();
                    } catch (AnimalCreationException e) {
                        System.out.println("Import error");
                        continue;
                    }
                    System.out.println("File import Ok");
                    for (Animal an : listAnimal)
                        System.out.println(an.toString());
                }
                default: {
                    lc.stopWork();
                    XmlImportExport.exportToFile(cages);
                    System.out.println("Have a good day, sir");
                    check = false;
                    break;
                }
            }
        }

    }

    public static void main(String[] args) {
       Configurator.defaultConfig()
                .writer(new FileWriter("log.txt"))
                .level(Level.DEBUG)
                .addWriter(new ConsoleWriter())
                .activate();
        MyLogger.log("start");
        Logger.info("Hello TinyLog");

        new Main();


        scan.close();
    }

    public static void displayAnimal(Object[] animals) {
        for (int i = 0; i < animals.length; i++) {
            System.out.println("#" + (i + 1) + ": " + animals[i].toString());
        }
    }

    public static void makeSound(Soundable[] sound) {
        for (int i = 0; i < sound.length; i++) {
            sound[i].sound();
        }
    }

    private void jumpAll() {
        StringBuilder sb = new StringBuilder("\n \tlet's jump\n");
        for (String key : cages.keySet()) {
            sb.append(key).append(" cage\n");
            if (cages.get(key).getCage().size() == 0) {
                sb.append("\tYour cage is empty\n");
            } else {
                double temp = cages.get(key).getCage().get(0).jump();
                int tempInd = 0;
                for (int i = 1; i < cages.get(key).getCage().size(); i++) {
                    if (cages.get(key).getCage().get(i).getSize() > temp) {
                        tempInd = i;
                    }
                }
                sb.append("The animal with the largest jump range is # ").
                        append((tempInd + 1)).
                        append(cages.get(key).getCage().get(tempInd).toString()).append("\n");
            }
        }
        System.out.println(sb.append("\n").toString());
    }

    private Animal createAnimal() throws AnimalCreationException {
        System.out.println("Synthesize wolf(1), cat(2), raven(3), rabbit(4) or exit(0)?");
        Animal temp;
        while (true) {
            String choice = scan.nextLine();
            switch (choice) {
                case ("1"): {
                    temp = ForestWolf.createWolf();
                    return temp;
                }
                case ("2"): {
                    temp = DomesticCat.createCat();
                    return temp;
                }
                case ("3"): {
                    temp = Raven.createRaven();
                    return temp;
                }
                case ("4"): {
                    temp = Rabbit.createRabbit();
                    return temp;
                }
                case ("0"): {
                    return null;
                }
                default: {
                    System.out.println("Wrong action, try again");
                }
            }
        }
    }

    private void showAnimalInfo() {
        StringBuilder sb = new StringBuilder("\n \tOutput information about cages\n");
        for (String key : cages.keySet()) {
            sb.append(key).append(" cage\n");
            if (cages.get(key).getCage().size() == 0) {
                sb.append("\tYour cage is empty\n");
            } else {
                sb.append(cages.get(key).getCageInfo());
            }
        }
        System.out.println(sb.append("\n").toString());
    }

    private ExtensibleCage chooseCageToChange() {
        while (true) {
            System.out.print("\n" +
                    "1. Mammal cage\n" +
                    "2. Bird cage\n" +
                    "3. Herbivore cage\n");
            String choice = scan.nextLine();
            switch (choice) {
                case "1": {
                    return cages.get(Mammal.class.getSimpleName());
                }
                case "2": {
                    return cages.get(Bird.class.getSimpleName());
                }
                case "3": {
                    return cages.get(Herbivore.class.getSimpleName());
                }
                default: {
                    System.out.println("Wrong cage #, try again");
                    break;
                }
            }
        }
    }

    private void sortToCages() {
        Animal temp = null;
        try {
            temp = createAnimal();
        } catch (AnimalInvalidNameException e) {
            System.out.println(e.getMessage());
        } catch (AnimalInvalidSizeException e) {
            System.out.println(e.getMessage());
        } catch (AnimalCreationException e) {
            System.out.println(e.getMessage());
        }
        if (temp != null) {
            if (temp instanceof Predator) {
                System.out.println("Your current mammal cage size is " +
                        cages.get(Mammal.class.getSimpleName()).addAnimal(temp));
            } else if (temp instanceof Bird) {
                System.out.println("Your current bird cage size is " +
                        cages.get(Bird.class.getSimpleName()).addAnimal(temp));
            } else if (temp instanceof Herbivore) {
                if (Math.random() >= 0.5) {
                    System.out.println("Your current mammal cage size is " +
                            cages.get(Mammal.class.getSimpleName()).addAnimal(temp));
                    cages.get(Mammal.class.getSimpleName()).checkHuntConditions(temp);
                } else {
                    System.out.println("Your current herbivore cage size is " +
                            cages.get(Herbivore.class.getSimpleName()).addAnimal(temp));
                }
            }
        }
    }

}
