package animals;

import error.AnimalCreationException;
import error.AnimalInvalidNameException;
import error.AnimalInvalidSizeException;
import input.Input;

public final class DomesticCat extends Felinae {
    private String breed;
    private final String sound = "meow";

    public DomesticCat(Double size, String nickname) {
        super(size, nickname);
        type = DomesticCat.class.getSimpleName();
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    @Override
    public void sound() {
        System.out.println(sound);
    }

    @Override
    public double jump() {
        return 100 / getSize();
    }

    public static DomesticCat createCat()  throws AnimalCreationException {
        double size = Input.megaInputNumber("Enter your cat's size");
        if (size <= 0){
            throw new AnimalInvalidSizeException();
        }
        String name = Input.megaInputString("Enter your cat's name");
        if (name.isEmpty()){
            throw new AnimalInvalidNameException();
        }
        return new DomesticCat(size, name);
    }
}
