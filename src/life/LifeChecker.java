package life;

import animals.Animal;
import animals.Mammal;
import main.ExtensibleCage;

import java.util.HashMap;
import java.util.Map;

public class LifeChecker extends Thread {
    private static LifeChecker lc;
    private Map<String, ExtensibleCage<? extends Animal>> map;
    private volatile Boolean isRun = true;

    private LifeChecker() {

    }

    public void setMap(Map<String, ExtensibleCage<? extends Animal>> map) {
        this.map = map;
    }

    public void startWork(Map<String, ExtensibleCage<? extends Animal>> map) {
        setMap(map);
        start();
    }

    public void stopWork() {
        isRun = false;
    }

    public static LifeChecker getLifeChecker() {
        synchronized (LifeChecker.class) {
            if (lc == null) {
                lc = new LifeChecker();

            }
            return lc;
        }
    }

    @Override
    public void run() {
        long timeStart = System.currentTimeMillis();
        int secondsFromStart = 0;
        while (isRun) {
            for (String key : map.keySet()) {
                ExtensibleCage<? extends Animal> cage = map.get(key);
                for (Animal animal : cage.getCage()) {
                    long timeAfter = System.currentTimeMillis();
                    if (timeAfter - timeStart >= 1000) {
                        animal.setFill(animal.getFill() - 1);
                        timeStart = timeAfter;
                        secondsFromStart++;
                        if(animal.getFill()<=20& !animal.isStarving()){
                            animal.setStarving(true);
                            System.out.println("Warning, animal "+animal.toString()+" starving");
                        }
                        if(animal.getFill() <=0){
                            animal.die();
                        }
                    }
                }
            }

        }
    }
}
