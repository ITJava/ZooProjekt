package animals;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import error.AnimalCreationException;
import food.Food;
import interfaces.Jumpable;
import interfaces.Soundable;
import meta.FillRetention;
import org.pmw.tinylog.Logger;

public abstract class Animal implements Soundable, Jumpable, Comparable<Animal> {
    public static final double INITIAL_FILL = 25;
    protected String type;
    protected double fill;
    protected long createdAt;
    protected long lastFeedTime;
    boolean isAlive = true;
    private double size;
    private String nickname;
    private IDeadAnimalListener deadAnimalListener;
    private boolean isStarving;

    public Animal(double size, String nickname) {
        this.size = size;
        this.nickname = nickname;
        setFill(INITIAL_FILL);
        this.createdAt = System.currentTimeMillis();
    }

    public void die() {
        this.isAlive = false;
        if (deadAnimalListener != null) {
            deadAnimalListener.onAnimalDeath(this);
        }
    }

    @Override
    public String toString() {
        return (getType() + "\t" + getNickname() + "\t" + getSize() + "\t"+getFill());
    }

    public int compareTo(Animal o) {
        return (int) Math.ceil(this.getSize() - o.getSize());
    }

    public final String getType() {
        return type;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public double getFill() {
        return fill;
    }

    public boolean isStarving() {
        return isStarving;
    }

    public void setStarving(boolean starving) {
        isStarving = starving;
    }

    public void setFill(double fill) {
        this.fill = fill;
        lastFeedTime = System.currentTimeMillis();
    }

    public void setDeadAnimalListener(IDeadAnimalListener deadAnimalListener) {
        this.deadAnimalListener = deadAnimalListener;
    }

    public double feed(Food food) {
        switch (food) {
            case CARROT:
                System.out.println("tasty carrot");
                break;
            case MEAT:
                System.out.println("tasty meat");
                break;
        }
        setFill(getFill() + food.getEnergyValue());
        return getFill();
    }

    public static Animal convertFromString(String input) throws AnimalCreationException {
        String[] values = input.split(",");
        switch (values[0]) {
            case "Cat":
                return new DomesticCat(Double.parseDouble(values[2]), values[1]);
            case "Wolf":
                return new ForestWolf(Double.parseDouble(values[2]), values[1]);
            case "Raven":
                return new Raven(Double.parseDouble(values[2]), values[1]);
            case "Rabbit":
                return new Rabbit(Double.parseDouble(values[2]), values[1]);
        }
        throw new AnimalCreationException();
    }

    public interface IDeadAnimalListener {
        void onAnimalDeath(Animal animal);
    }

    @SuppressWarnings("unchecked")
    public static <ANIMAL_TYPE extends Animal> ANIMAL_TYPE getAnimal(String type, double size, String nickName) throws AnimalCreationException {
        try {
            Class<ANIMAL_TYPE> clazz = (Class<ANIMAL_TYPE>) Class.forName(type);
            Class<?> cls[] = new Class[]{Double.class, String.class};
            Constructor<ANIMAL_TYPE> c = clazz.getConstructor(cls);
            ANIMAL_TYPE obj = c.newInstance(new Double(size), nickName);
            return obj;
        } catch (ClassNotFoundException e) {
            throw new AnimalCreationException("ClassNotFoundException : " + e.getMessage());
        } catch (ClassCastException e) {
            throw new AnimalCreationException("ClassCastException : " + e.getMessage());
        } catch (InstantiationException e) {
            throw new AnimalCreationException("InstantiationException : " + e.getMessage());
        } catch (IllegalAccessException e) {
            throw new AnimalCreationException("IllegalAccessException : " + e.getMessage());
        } catch (NoSuchMethodException e) {
            throw new AnimalCreationException("NoSuchMethodException : " + e.getMessage());
        } catch (SecurityException e) {
            throw new AnimalCreationException("SecurityException : " + e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new AnimalCreationException("IllegalArgumentException : " + e.getMessage());
        } catch (InvocationTargetException e) {
            throw new AnimalCreationException("InvocationTargetException : " + e.getMessage());
        }
    }
}
