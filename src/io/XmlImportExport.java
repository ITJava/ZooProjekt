package io;

import animals.Animal;
import animals.Bird;
import animals.Herbivore;
import animals.Mammal;
import error.AnimalCreationException;
import main.ExtensibleCage;
import meta.FillRetention;
import org.pmw.tinylog.Logger;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class XmlImportExport {
    final static String ELEMENT_NAME_ROOT = "ZOO";
    final static String ELEMENT_NAME_CAGE = "CAGE";
    final static String ELEMENT_NAME_ANIMAL = "ANIMAL";
    final static String ATRIBUTE_NAME_TYPE = "type";
    final static String ATRIBUTE_NAME_FILL = "fill";
    final static String ATRIBUTE_NAME_SIZE = "size";

    public static void exportToFile(Map<String, ExtensibleCage<? extends Animal>> cages) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();
            Element rootElement = doc.createElement(ELEMENT_NAME_ROOT);
            doc.appendChild(rootElement);
            for (String key : cages.keySet()) {
                ExtensibleCage<? extends Animal> cage = cages.get(key);
                Element cageElement = doc.createElement(ELEMENT_NAME_CAGE);
                rootElement.appendChild(cageElement);
                Attr cageTypeAttribute = doc.createAttribute(ATRIBUTE_NAME_TYPE);
                cageTypeAttribute.setValue(key);
                cageElement.setAttributeNode(cageTypeAttribute);
                for (Animal animal : cage.getCage()) {
                    Element animalElement = doc.createElement(ELEMENT_NAME_ANIMAL);
                    Attr animalTypeAttribute = doc.createAttribute(ATRIBUTE_NAME_TYPE);
                    animalTypeAttribute.setValue(animal.getType());
                    animalElement.setAttributeNode(animalTypeAttribute);
                    Attr animalFillAttribute = doc.createAttribute(ATRIBUTE_NAME_FILL);
                    animalFillAttribute.setValue(String.valueOf(animal.getFill()));
                    animalElement.setAttributeNode(animalFillAttribute);
                    Attr animalSizeAttribute = doc.createAttribute(ATRIBUTE_NAME_SIZE);
                    animalSizeAttribute.setValue(String.valueOf(animal.getSize()));
                    animalElement.setAttributeNode(animalSizeAttribute);
                    animalElement.appendChild(doc.createTextNode(animal.getNickname()));
                    cageElement.appendChild(animalElement);
                }
            }
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(".autosave"));
            transformer.transform(source, result);

        } catch (ParserConfigurationException e) {
            System.out.println(e.getMessage());
        } catch (TransformerConfigurationException e) {
            System.out.println(e.getMessage());
        } catch (TransformerException e) {
            System.out.println(e.getMessage());
        }
    }

    public static Map<String, ExtensibleCage<? extends Animal>> loadFromXml() {
        Map<String, ExtensibleCage<? extends Animal>> map = new ConcurrentHashMap<>();
        try {
            File inputFile = new File(".autosave");
            if (!inputFile.exists()) {
                map.put(Mammal.class.getSimpleName(), new ExtensibleCage<Mammal>());
                map.put(Bird.class.getSimpleName(), new ExtensibleCage<Bird>());
                map.put(Herbivore.class.getSimpleName(), new ExtensibleCage<Herbivore>());
                return map;

            }
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName(ELEMENT_NAME_CAGE);
            ExtensibleCage<? extends Animal> cage = null;
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node cageNode = nList.item(temp);
                if (cageNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) cageNode;
                    String cageType = element.getAttribute(ATRIBUTE_NAME_TYPE);

                    switch (cageType) {
                        case "Mammal":
                            cage = new ExtensibleCage<Mammal>();
                            map.put(Mammal.class.getSimpleName(), cage);
                            break;
                        case "Bird":
                            cage = new ExtensibleCage<Bird>();
                            map.put(Bird.class.getSimpleName(), cage);
                            break;
                        case "Herbivore":
                            cage = new ExtensibleCage<Herbivore>();
                            map.put(Herbivore.class.getSimpleName(), cage);
                            break;
                        default:
                            continue;
                    }
                }
                NodeList animalList = cageNode.getChildNodes();
                for (int i = 0; i < animalList.getLength(); i++) {
                    Node animalNode = animalList.item(i);
                    if (animalNode.getNodeType() == animalNode.ELEMENT_NODE) {
                        String nodeName = animalNode.getNodeName();
                        String nickName = animalNode.getChildNodes().item(0).getTextContent();
                        String size = ((Element) animalNode).getAttribute(ATRIBUTE_NAME_SIZE);
                        Double fill = Double.valueOf(((Element) animalNode).getAttribute(ATRIBUTE_NAME_FILL));
                        String type = ((Element) animalNode).getAttribute(ATRIBUTE_NAME_TYPE);
                        try {
                            Animal animal = Animal.getAnimal("animals." + type, Double.parseDouble(size), nickName);
                            for (Annotation annotation : animal.getClass().getAnnotations()) {
                                if (annotation instanceof FillRetention) {
                                    Logger.warn("We found annotation");
                                    fill = Animal.INITIAL_FILL;
                                }
                            }
                            animal.setFill(fill);
                            cage.addAnimal(animal);
                        } catch (AnimalCreationException e) {
                            System.out.println(e.getMessage());
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return map;
    }
}